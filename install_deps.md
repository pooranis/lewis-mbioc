# Installing QIIME 2

Lewis, if you have not already installed miniconda (or Anaconda), download the pkg installer here:
https://docs.conda.io/en/latest/miniconda.html
You should just be able to double click it.
https://conda.io/projects/conda/en/latest/user-guide/install/macos.html

Once conda is installed, follow these instructions to install qiime2: https://docs.qiime2.org/2019.7/install/native/
You will need to type the commands in the terminal.

Once you have installed qiime2, you can ping me, and we do the rest together, as it only takes a minute. But, if you are feeling adventurous, activate the environment and install the following:

```bash
conda activate qiime2-2019.7
conda install -c conda-forge calysto_bash
pip install wurlitzer
```

When the environment is activated, you should see a little `(qiime2-2019.7)` next to your bash prompt. To get out of the qiime2 environment, I usually just close the terminal window I'm using. But, you can also deactivate (I find that deactivating still leaves behind some crud in my environment which I dislike, but this is probably just my own neurosis):

```bash
conda deactivate
```

## Installing JupyterLab

To use the jupyter notebook, it's best to install jupyter lab outside of qiime2.  Then, potentially, if you wanted to install other tools/environments/kernels, you could also use those with jupyter.  To install and run jupyterlab:

```bash
# make sure you are not in the qiime2 environment
conda install -y nodejs jupyterlab nb_conda_kernels
jupyter lab
```

It should automatically open jupyter lab in the browser. I'm attaching the notebook to run ANCOM here [bash.ipynb](bash.ipynb)

If this is too difficult, maybe we can ask @macmenaminpe to help set up the security stuff for an AWS instance so you can access the jupyter lab from the web. We can use the new qiime2 ami, and I can install the few extra things needed to get jupyter lab going. It would be a nice thing to have in general...
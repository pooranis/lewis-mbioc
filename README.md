# lewis-mbioc
Lewis' MBIOC Conference Poster

- [Installing QIIME 2](install_deps.md) - or use [docker container](https://hub.docker.com/r/pooranis/lewis-mbioc-qiime2)
- [Bash Notebook](bash.ipynb) to run [ANCOM](https://docs.qiime2.org/2019.7/plugins/available/composition/ancom/) to find differentially abundant organisms between groups.

### Data

Upload your input data (biom file, taxonomy table, mapping file) to the [data](data) directory.